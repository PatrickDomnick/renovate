module.exports = {
    endpoint: 'https:/gitlab.com/api/v4/',
    platform: 'gitlab',
    persistRepoData: true,
    logLevel: 'debug',
    autodiscover: true,
    autodiscoverFilter: "PatrickDomnick/gin-vals"
};
